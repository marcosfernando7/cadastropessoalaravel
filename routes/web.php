<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'pessoa.index', 'uses'=>'PessoasDadosController@index']);

// Route::get('/list', ['as'=>'pessoa.list', 'uses'=>'PessoasDadosController@list']);


// Exclusao Cliente
Route::delete('/pessoa/{id}/destroy', ['as'=>'pessoa.destroy', 'uses'=>'PessoasDadosController@destroy']);


// get clientes api
Route::get('/api/clientes', ['as'=>'api.clientes', 'uses'=>'PessoasDadosController@get_clientes']);
Route::post('/api/cep', ['as'=>'api.cep', 'uses'=>'PessoasDadosController@get_cep']);

// Pessoa Física
// insercão
Route::post('/pessoa-fisica/store', ['as'=>'pessoa_fisica.store', 'uses'=>'PessoaFisicaController@store']);

// Pessoa Jurídica
// inserção
Route::post('pessoa-juridica/store', ['as'=>'pessoa_juridica.store', 'uses'=>'PessoaJuridicaController@store']);