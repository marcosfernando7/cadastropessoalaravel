// pessoa fisica
$('#modal_pessoa_fisica').click(function(){
    $('.error_pessoa').hide();
    $('#pessoaFisica').modal('show');

    // $('.tipo_pessoa').val('pessoa_fisica');

    $('.cep').val('');
    $('.nascimento').val('');
    $('.complemento').val('');
    $('.bairro').val('');
    $('.logradouro').val('');
    $('.cidade').val('');
    $('.uf').val('');
});

$('#pessoaFisica').on('shown.bs.modal', function() {
    $(this).find('[autofocus]').focus();
});


// pessoa juridica
$('#modal_pessoa_juridica').click(function(){
    $('.error_pessoa').hide();
    $('#pessoaJuridica').modal('show');

    // $('.tipo_pessoa').val('pessoa_juridica');

    $('.cep').val('');
    $('.nascimento').val('');
    $('.complemento').val('');
    $('.bairro').val('');
    $('.logradouro').val('');
    $('.cidade').val('');
    $('.uf').val('');
});

$('#pessoaJuridica').on('shown.bs.modal', function() {
    $(this).find('[autofocus]').focus();
});


// cep pessoa fisica
$('.cep').change(function(){

    var cep = $(this).val();

    var cepfinal = cep.replace('-', '');

    var request = $.ajax({
        url: '/api/cep',
        type: 'post',
        dataType: 'json',
        data: { _token: $('input[name="_token"]').val(), cep: cepfinal}
    });

    request.done(function(e){
        $('.bairro').val(e.neighborhood);
        $('.logradouro').val(e.street);
        $('.cidade').val(e.city);
        $('.uf').val(e.state);
    });

    request.fail(function(e){
        $('.cep').val('');
        $('.bairro').val('');
        $('.logradouro').val('');
        $('.cidade').val('');
        $('.uf').val('');
        alert('Nenhum endereço foi localizado com esse CEP!');
    });

});


// excluir cliente modal
$('.excluir').click(function(){
    var id_cliente = $(this).attr('data-id');
    $('#id_pessoa_delete').val(id_cliente);
    $('#modalExcluir').modal('show');

});


// delete cliente ajax
$('#pessoa_delete').submit(function(){
    var id_pessoa = $('#id_pessoa_delete').val();

    var request = $.ajax({
        url: '/pessoa/' + id_pessoa + '/destroy',
        type: 'delete',
        data: { _token: $('input[name="_token"]').val(), id: id_pessoa}
    });

    request.done(function(e){
        window.location='/';
    });

    request.fail(function(e){
        console.log(e);
    });

    return false;
});


// masks
$('.cpf').mask("999.999.999-99");
$('.nascimento').mask("99/99/9999");
$('.cep').mask('99999-999');
$('.cnpj').mask('99.999.999/9999-99');