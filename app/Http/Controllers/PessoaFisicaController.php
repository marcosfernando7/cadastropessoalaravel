<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\PessoaFisica;
use App\PessoasDados;

// Request
use App\Http\Requests\PessoaFisicaFormResquest;

class PessoaFisicaController extends Controller
{
    public function store(PessoaFisicaFormResquest $request){

            $pessoadados = new PessoasDados();
            $pessoadados->nome           = $request->nome;
            $pessoadados->cep            = tratar_cep($request->cep);
            $pessoadados->logradouro     = $request->logradouro;
            $pessoadados->numero         = $request->numero;
            $pessoadados->complemento    = $request->complemento;
            $pessoadados->bairro         = $request->bairro;
            $pessoadados->cidade         = $request->cidade;
            $pessoadados->uf             = $request->uf;

            $pessoadados->save();
            $pessoa_id = $pessoadados->id_pessoa;

            if($pessoa_id) :
                $pessoafisica = new PessoaFisica();
                $pessoafisica->sobrenome      = $request->sobrenome;
                $pessoafisica->cpf            = tratar_cpf($request->cpf);
                $pessoafisica->nascimento     = data_db($request->nascimento);
                $pessoafisica->pessoa_id      = $pessoa_id;

                // salvar dados
                $pessoafisica->save();
            endif;

            // mensagem de retorno
            $request->session()->flash(
                'mensagem',
                "O cliente {$request->nome} foi inserido com Sucesso!"
            );

        // redireciona
        return redirect()->route('pessoa.index');
    }
}
