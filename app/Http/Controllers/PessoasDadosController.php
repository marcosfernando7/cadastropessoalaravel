<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PessoaJuridica;
use App\PessoaFisica;
use App\PessoasDados;
use JansenFelipe\CepGratis\CepGratis;

use DB;

class PessoasDadosController extends Controller
{
    public function index(Request $request){

        $clientes = DB::table('pessoas_dados as pd')
                ->leftJoin('pessoa_fisica as pf', 'pf.pessoa_id', '=', 'pd.id_pessoa')
                ->leftJoin('pessoa_juridica as pj', 'pj.pessoa_id', '=', 'pd.id_pessoa')
                    ->select('id_pessoa',
                             'pd.nome',
                             'pf.cpf',
                             'pj.cnpj',
                             'pd.cidade'
                             )
                    ->orderBy('pd.nome')
                    ->paginate(3);

        $mensagem = $request->session()->get('mensagem');
        $request->session()->remove('mensagem');

        return view('index', compact('mensagem', 'clientes'));

    }

    public function get_clientes(){

        $sql = "SELECT id_pessoa,
                        nome,
                        sobrenome,
                        cpf,
                        cnpj,
                        nascimento,
                        razao_social,
                        cep,
                        bairro,
                        logradouro,
                        numero,
                        complemento,
                        bairro,
                        cidade,
                        uf
                        from pessoas_dados pd
                            left join pessoa_fisica pf
                                on pd.id_pessoa = pf.pessoa_id
                            left join pessoa_juridica pj
                                on pj.pessoa_id = pd.id_pessoa
                                    order by pd.id_pessoa";

        $clientes = DB::select($sql);

        return response()->json($clientes, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
         JSON_UNESCAPED_UNICODE);
    }

    // Api CEP
    public function get_cep(Request $request){

        $cep = $request->cep;

        $address = CepGratis::search($cep);

        if($address) :
            return response()->json($address);
        endif;
    }

    // Exclusão Pessoa
    public function destroy($id){

        $cliente = PessoasDados::find($id);
        $cliente->delete();

    }

    // public function list(){
    //     $pessoa = PessoasDados::find(8)->pessoa_fisica;

    //     dd($pessoa);
    // }

}