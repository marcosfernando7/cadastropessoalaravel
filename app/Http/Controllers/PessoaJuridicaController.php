<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\PessoaJuridica;
use App\PessoasDados;

// Request
use App\Http\Requests\PessoaJuridicaFormRequest;

class PessoaJuridicaController extends Controller
{
    public function store(PessoaJuridicaFormRequest $request){

        $pessoadados = new PessoasDados();
        $pessoadados->nome           = $request->nome;
        $pessoadados->cep            = tratar_cep($request->cep);
        $pessoadados->logradouro     = $request->logradouro;
        $pessoadados->numero         = $request->numero;
        $pessoadados->complemento    = $request->complemento;
        $pessoadados->bairro         = $request->bairro;
        $pessoadados->cidade         = $request->cidade;
        $pessoadados->uf             = $request->uf;

        $pessoadados->save();
        $pessoa_id = $pessoadados->id_pessoa;

        if($pessoa_id) :
            $pessoajuridica = new PessoaJuridica();
            $pessoajuridica->cnpj           = tratar_cnpj($request->cnpj);
            $pessoajuridica->razao_social   = $request->razao_social;
            $pessoajuridica->pessoa_id      = $pessoa_id;

            // salvar dados
            $pessoajuridica->save();
        endif;

        // mensagem de retorno
        $request->session()->flash(
            'mensagem',
            "O cliente {$request->nome} foi inserido com Sucesso!"
        );


    // redireciona
    return redirect('./');

    }
}
