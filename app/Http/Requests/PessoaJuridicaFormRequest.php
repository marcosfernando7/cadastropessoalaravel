<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PessoaJuridicaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'          => 'required|min:3',
            'cnpj'          => 'required|min:18',
            'razao_social'  => 'required|min:3',
            'logradouro'    => 'required|min:3',
            'cep'           => 'required',
            'numero'        => 'required',
            'complemento'   => 'nullable|min:3',
            'bairro'        => 'required|min:3',
            'cidade'        => 'required|min:3',
            'uf'            => 'required'
        ];
    }

    public function messages(){
        return [
            'required'                  => 'O campo :attribute é obrigatório!',
            'min'                       => 'O campo :attribute deve ter pelo menos 3 caracteres!',
            'cnpj.min'                  => 'Preeencha o campo CNPJ conrretamente!'
        ];
    }
}
