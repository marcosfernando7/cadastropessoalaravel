<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PessoaFisicaFormResquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            'nome'                  => 'required|min:3',
            'sobrenome'             => 'required|min:3',
            'cpf'                   => 'required|unique:pessoa_fisica|min:14',
            'cep'                   => 'required',
            'nascimento'            => 'required|min:10|date_format:d/m/Y|before:-19 years',
            'logradouro'            => 'required|min:3',
            'numero'                => 'required',
            'complemento'           => 'nullable|min:3',
            'bairro'                => 'required|min:3',
            'cidade'                => 'required|min:3',
            'uf'                    => 'required'
        ];

    }

    public function messages(){
        return [
            'required'                  => 'O campo :attribute é obrigatório!',
            'unique'                    => 'O valor digitado no campo :attribute já foi cadastrado!',
            'min'                       => 'O campo :attribute deve ter pelo menos 3 caracteres!',
            'cpf.min'                   => 'Preencha o campo :attribute corretamente!',
            'date_format'               => 'Preencha o campo data de :attribute corretamente!',
            'nascimento.before'         => 'A idade mínima permitida é de 19 anos'
        ];
    }

}
