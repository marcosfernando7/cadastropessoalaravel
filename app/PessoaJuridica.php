<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PessoaJuridica extends Model
{
    protected $table = 'pessoa_juridica';
    protected $primaryKey = 'id_pessoa_juridica';

    public function pessoa(){
        return $this->belongsTo('App\PessoasDados');
    }
}
