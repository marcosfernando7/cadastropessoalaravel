<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PessoaFisica extends Model
{
    protected $primaryKey = 'id_pessoa_fisica';
    protected $table = 'pessoa_fisica';

    public function pessoa(){
        return $this->belongsTo('App\PessoasDados');
    }
}
