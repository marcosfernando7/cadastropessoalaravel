<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PessoasDados extends Model
{
    protected $primaryKey = 'id_pessoa';
    protected $table = 'pessoas_dados';

    public function pessoa_fisica(){
        return $this->hasOne('App\PessoaFisica', 'pessoa_id');
    }

    public function pessoa_juridica(){
        return $this->hasOne('App\PessoJuridica', 'pessoa_id');
    }
}
