<?php
date_default_timezone_set('America/Sao_Paulo');

function data_db($value){
    $data = implode("-", array_reverse(explode("/", $value)));
	return $data;
}

function tratar_cpf($value){
    $cpf = str_replace(array('.', '-'), array('', ''), $value);
    return $cpf;
}

function tratar_cnpj($value){
    $cnpj = str_replace(array('.', '-', '/'), array('', '', ''), $value);
    return $cnpj;
}

function tratar_cep($value){
    $cep = str_replace('-', '', $value);
    return $cep;
}

function tratar_tipo($cpf){
    if(!empty($cpf)){
        echo "<span class='badge badge-success'>Pessoa Física<span>";
    }else {
        echo "<span class='badge badge-primary'>Pessoa Jurídica<span>";
    }
}

function mask($val, $mask){
	$maskared = '';
	$k = 0;
	for($i = 0; $i<=strlen($mask)-1; $i++)
	{
		if($mask[$i] == '#')
		{
			if(isset($val[$k]))
				$maskared .= $val[$k++];
		}
		else
		{
			if(isset($mask[$i]))
				$maskared .= $mask[$i];
		}
	}
	return $maskared;
}