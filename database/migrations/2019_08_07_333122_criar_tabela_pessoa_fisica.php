<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaPessoaFisica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('pessoa_fisica', function(Blueprint $table){
            $table->Increments('id_pessoa_fisica');
            // $table->string('nome');
            $table->string('sobrenome', 15);
            $table->string('cpf', 11);
            $table->date('nascimento');
            $table->unsignedInteger('pessoa_id')->index();
            $table->foreign('pessoa_id')->references('id_pessoa')->on('pessoas_dados')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pessoa_fisica');
    }
}
