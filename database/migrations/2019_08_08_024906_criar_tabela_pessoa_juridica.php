<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaPessoaJuridica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoa_juridica', function(Blueprint $table){
            $table->Increments('id_pessoa_juridica');
            $table->string('cnpj', 14);
            $table->string('razao_social');
            $table->unsignedInteger('pessoa_id')->index();
            $table->foreign('pessoa_id')->references('id_pessoa')->on('pessoas_dados')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pessoa_juridica');
    }
}
