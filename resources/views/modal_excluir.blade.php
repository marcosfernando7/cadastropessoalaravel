<!-- Modal -->
<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-trash"></i> Exclusão</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="" method="post" id="pessoa_delete">
        <div class="modal-body">
            <h4>Deseja realmente excluir este cliente?</h4>
            <input type="hidden" id="id_pessoa_delete" name="id_pessoa_delete">
            {!! csrf_field() !!}
            {{ method_field('DELETE') }}
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Sim</button>
            </div>
        </form>
    </div>
</div>
</div>