@extends('layout')

@section('title')
Cadastro de Clientes - Laravel
@endsection

@section('header')
<i class="fa fa-user-o"></i> Cadastro de Clientes
@endsection

@section('content')

@if(isset($mensagem))
<div class="alert alert-success">
    <i class="fa fa-check-circle"></i> {{ $mensagem }}
</div>
@endif

@include('modal_excluir')

<h2>Selecione o tipo:</h2>

<div class="row">
    <div class="col-md-12 form-group">
    <a href="#" class="btn btn-primary" id="modal_pessoa_fisica"><i class="fa fa-user-plus"></i> Pessoa Física</a>
    <a href="#" class="btn btn-primary" id="modal_pessoa_juridica"><i class="fa fa-user-plus"></i> Pessoa Jurídica</a>

        <div class="pull-right">
        <a href="{{ route('api.clientes') }}" target="_blank" class="btn btn-light" id="modal_pessoa_juridica"><i class="fa fa-code"></i> API (JSON)</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 form-group">
        <div class="table-responsive">
        <table class="table table-striped table-sm table-hover">
            <tr>
                <th>#</th>
                <th>Tipo</th>
                <th>Nome/Nome Fantasia</th>
                <th>Cidade</th>
                <th>CPF</th>
                <th>CNPJ</th>
                <th><center>Excluir</center></th>
            </tr>

            @foreach($clientes as $cliente)
                <tr>
                    <td><small>{{ $cliente->id_pessoa }}</small></td>
                    <td>{{ tratar_tipo($cliente->cpf) }}</td>
                    <td>{{ $cliente->nome }}</td>
                    <td><small>{{ $cliente->cidade }}</small></td>
                    <td>
                        @if(!empty($cliente->cpf))
                            <small>{{ mask($cliente->cpf,'###.###.###-##') }}</small>
                        @endif
                    </td>

                    <td>
                        @if(!empty($cliente->cnpj))
                            <small>{{ mask($cliente->cnpj,'##.###.###/####-##') }}</small>
                        @endif
                    </td>

                    <td>
                        <center>
                        <a href="#" class="btn btn-sm btn-danger excluir" data-id="{{ $cliente->id_pessoa }}">
                                <i class="fa fa-trash"></i>
                            </a>
                        </center>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

        {!! $clientes->render("pagination::bootstrap-4") !!}

    </div>
</div>

@include('pessoa_fisica/create')
@include('pessoa_juridica/create')

@endsection

@section('scripts')
<script src="{{ asset('js/pessoas/app.js') }}"></script>
@if(count($errors) > 0))
    <script>
        $('#pessoaFisica').modal('show');
    </script>
@endif
@endsection