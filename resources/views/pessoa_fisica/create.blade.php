<!-- Modal -->
<div class="modal fade" id="pessoaFisica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-plus"></i> Cadastrar Pessoa Física</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">

        @include('../errors')

        <form method="post" action="{{ route('pessoa_fisica.store') }}">

        {!! csrf_field() !!}

    <div class="card">
        <div class="card-header">
            <h4><i class="fa fa-user-o"></i> Dados Pessoais</h4>
        </div>
    <div class="card-body">

    <div class="row">

        <div class="col-md-4 form-group">
            <label for="nome_pessoa_fisica">Nome <span class="text-danger"> *</span></label>
            <input type="text" name="nome" class="form-control form-control-sm" id="nome_pessoa_fisica" maxlength="255" autofocus>
        </div>

        <div class="col-md-3 form-group">
            <label for="sobrenome_pessoa_fisica">Sobrenome <span class="text-danger"> *</span></label>
            <input type="text" name="sobrenome" class="form-control form-control-sm" id="sobrenome_pessoa_fisica" maxlength="15">
        </div>

        <div class="col-md-3 form-group">
            <label for="cpf_pessoa_fisica">CPF <span class="text-danger"> *</span></label>
            <input type="text" name="cpf" class="form-control form-control-sm cpf" id="cpf_pessoa_fisica">
        </div>

        <div class="col-md-2 form-group">
            <label for="nascimento_pessoa_fisica">Data Nascimento <span class="text-danger"> *</span></label>
            <input type="text" name="nascimento" class="form-control form-control-sm nascimento" id="nascimento_pessoa_fisica">
        </div>
    </div>
</div>
</div>

@include('../form_dados_pessoas')

</form>
</div>
</div>
</div>