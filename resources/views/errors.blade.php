  {{-- traz as mensagens de erros (validacoes nos campos) --}}
  @if ($errors->any())
  <div class="alert alert-danger error_pessoa">
      @foreach ($errors->all() as $error)
          <small><i class="fa fa-exclamation-circle"></i> {{ $error }}</small> <br>
      @endforeach
  </div>
@endif