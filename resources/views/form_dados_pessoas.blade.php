        <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-envelope-o"></i> Dados para Correspondência</h4>
                </div>
            <div class="card-body">
            <div class="row l">
                <div class="col-md-4 form-group">
                    <label for="cep_pessoa">CEP <span class="text-danger"> *</span> <em>(Ex.: 15025-000)</em></label>
                    <input type="text" name="cep" class="form-control form-control-sm cep" id="cep">
                </div>
            </div>

            <div class="row">
                    <div class="col-md-4 form-group">
                        <label for="logradouro_pessoa">Logradouro <span class="text-danger"> *</span></label>
                        <input type="text" name="logradouro" class="form-control form-control-sm logradouro" id="logradouro">
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="numero_pessoa">Número <span class="text-danger"> *</span></label>
                        <input type="text" name="numero" class="form-control form-control-sm numero" id="numero">
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="complemento_pessoa">Complemento</label>
                        <input type="text" name="complemento" class="form-control form-control-sm complemento" id="complemento">
                    </div>
            </div>

            <div class="row">
                    <div class="col-md-4 form-group">
                        <label for="bairro_pessoa">Bairro <span class="text-danger"> *</span></label>
                        <input type="text" name="bairro" class="form-control form-control-sm bairro" id="bairro">
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="cidade_pessoa">Cidade <span class="text-danger"> *</span></label>
                        <input type="text" name="cidade" class="form-control form-control-sm cidade" id="cidade">
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="uf_pessoa">UF <span class="text-danger"> *</span></label>
                        <input type="text" name="uf" class="form-control form-control-sm uf" id="uf_pessoa" maxlength="2">
                    </div>
            </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-light" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
      <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
    </div>