<!-- Modal -->
<div class="modal fade" id="pessoaJuridica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-plus"></i> Cadastrar Pessoa Jurídica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            @include('../errors')

            <form method="post" action="{{ route('pessoa_juridica.store') }}">

            {!! csrf_field() !!}

            <div class="card">
                <div class="card-header">
                    <h4><i class="fa fa-user-o"></i> Dados da Pessoa Jurídica</h4>
                </div>
            <div class="card-body">

            <div class="row">

                <div class="col-md-4 form-group">
                    <label for="nome_pessoa_juridica">Nome Fantasia<span class="text-danger"> *</span></label>
                    <input type="text" name="nome" class="form-control form-control-sm" id="nome_pessoa_juridica" maxlength="255" autofocus>
                </div>

                <div class="col-md-3 form-group">
                    <label for="cnpj_pessoa_juridica">CNPJ <span class="text-danger"> *</span></label>
                    <input type="text" name="cnpj" class="form-control form-control-sm cnpj" id="cnpj_pessoa_juridica" maxlength="15">
                </div>

                <div class="col-md-3 form-group">
                    <label for="razao_social_pessoa_juridica">Razão Social <span class="text-danger"> *</span></label>
                    <input type="text" name="razao_social" class="form-control form-control-sm" id="razao_social_pessoa_juridica">
                </div>

            </div>
        </div>
        </div>

        @include('../form_dados_pessoas')

        </form>
    </div>
</div>
</div>